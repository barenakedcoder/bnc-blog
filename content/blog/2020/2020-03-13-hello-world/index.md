+++
title = 'Hello World'
slug = 'hello-world'
tags = ['blogging']
+++
<!-- In last launch, was 2018-09-26 -->
<!-- In last launch, was 2019-09-03 -->

Welcome to the re- re- re-launch of my blog.
<!--more-->
Born (again) in the midst of a
[global pandemic](https://en.wikipedia.org/wiki/2019%E2%80%9320_coronavirus_pandemic), 
I have enough free time to get this thing started
and (hopefully) build enough momentum to keep it going.

My primary goal is simple and selfish: improve my writing.
10+ years ago I wrote a lot more with a lot less effort.
Now, not as much and not as easily.

These I need to keep in mind:
A blog post is not a book.
Many short posts are better than zero brilliant long posts.
And the content needs to be only slightly better than what's already out there.

Now, to ~~work~~ fun ...
