+++
title = 'Font Awesome vs Material Icons'
tags = ['coding']
+++

Thank you [Font Awesome](https://fontawesome.com/) for many years of free awesome icons.
I would still use and even pay for you again.
But for websites that don't need all of your awesomeness, and that's many websites, 
[Material Icons](https://material.io/resources/icons/) is now my choice for icons.
<!--more-->

# About Font Awesome

[Font Awesome](https://fontawesome.com/) ([wiki](https://en.wikipedia.org/wiki/Font_Awesome)),
has been around since 2012.  It was originally built for use with Bootstrap
and was hosted on Bootstrap’s CDN too.

As of now, Font Awesome has 1535 free icons and an additional 5485 icons for the paid plan.

In the past, using Font Awesome was easy.
Just add a `<link>` to get a CSS file with the icons from their CDN site
and then start adding icons to your web pages.
Example:
```html
<i class="fas fa-camera"></i> 
```

Now, it's not as easy.
To use the icons hosted on their CDN site, you must sign up & create an account
(providing your email address) to get an embed code to include in your `<link>`.

Font Awesome has [free and paid plans](https://fontawesome.com/plans).
I assume (but couldn't confirm) these are for using the icons hosted on their CDN
(see below for another option).

Curiously, these plans have limits on page views per month:
10 thousand for the free plan and 1 million for the paid plan.
It's unclear what happens when the limit is exceeded.
Does it stop serving icons for the rest of the month?
I could not find an answer.
10K/m for a low volume site is okay.  But for anything busier, it might not.
10K/m is under 14 pages per hour on average: not a huge volume.

If you would rather not sign-up for a plan and/or worry about page view limits,
you can download Font Awesome (anonymously) and host it on your own web server.
This option is a little less documented, a little more work, but more worry-free.

# About Material Icons

[Material Icons](https://material.io/resources/icons/),
is an open source resource available from Google's
[Material Design](https://material.io/) project.

As of now, Material Icons has 1057 icons available in each of 5 themes:
filled, outlined, rounded, two-tone, and sharp.

That may seem less than Font Awesome's 1535 free icons.
But Font Awesome's 1535 includes icons that Material Icons has split into themes.
Both the filled and outlined versions of an icon, for example, would be in Font Awesome's 1535
but would be 1 icon in Material Icons's set of 1057 filled icons
and 1 icon in Material Icons's set of 1057 outlined icons.
So, it's unclear who has the most icons.

With 1057+ icons, there's a good chance you'll find what you need with Material Icons.

Font Awesome's paid plan, with 5485 icons, has icons for even more obscure needs
(ex [chess](https://material.io/)) and so it is worth paying for if needed.

Using Material Icons today is as easy as using Font Awesome was in the past.
No sign up required.  Just add the `<link>` and go.
If your web page is already making a request for [Google Fonts](https://fonts.google.com/),
you can piggyback on that request to get the icons.  Here's an example:
```html
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans|Material+Icons&display=swap">
```

The interesting part is near the end of the `href`
(you may need to scroll right to see it).
By adding `Material+Icons` after `Open+Sans`, you get the icons in the same request
as getting your font, Open Sans in this case.
(Well, not *exactly* the same web request but that's beyond the scope of this post.)

Unlike for CDN-hosted Font Awesome, an additional
[TLS handshake](https://www.cloudflare.com/learning/ssl/what-happens-in-a-tls-handshake/)
is not required.  This makes your web page a little faster.

If you prefer, you can download Material Icons (anonymously) and host it on your own web server.
Get it from [Material Icon's github repo.](https://github.com/google/material-design-icons).

# Evaluation

As always, it depends on your needs.
First, you should determine your needs: which icons you cannot live without in your app.
Remember, less is more.

If Material Icons provides those icons, use it.
Since you're probably using a Google Font anyway, get your icons too without an additional
[TLS handshake](https://www.cloudflare.com/learning/ssl/what-happens-in-a-tls-handshake/).

If you need icons Material Icons does not provide, you'll need to look elsewhere.
Font Awesome is a great place to look.
And they deserve your business too for all the awesomeness they have given the web over the years.
