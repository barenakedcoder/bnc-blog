+++
title = 'Unobtrusive Monetization - Part 1'
tags = ['blogging']
+++

For gits and shiggles, I thought I would experiment by adding some unobtrusive monetization to this blog.
In Part 1, I describe what I added.
In Part 2, some months from now, I'll describe the results.
<!--more-->

# Needs
Every now and then I read a blog that really helps me.
Something that gives me some value in either time saved or knowledge gained.
Sometimes it's something I wouldn't mind giving back a little to show appreciation for the author's efforts.

Although this blog is very new, I have hopes it will produce something its reader(s) will value.
If that ever happens, it would be interesting to see if any reader does give back something given a way to do so.
So …

I need "a way" for my reader(s) to donate a small amount, if they so choose.
This "way" must somehow get something of value (money, preferably) from the reader to me.

# Candidates
Since this wasn't a decision of any importance, I did not do a thorough search for and evaluation of candidates.
Instead, I did it quickly, narrowly, and shallowly.  Apologies if I missed your favourite.

I looked at [PayPal](https://www.paypal.com/ca/non-profit/fundraising/individuals),
the usual suspect, but it seems to target registered charities and was not otherwise a great fit.
Same for [donorbox](https://donorbox.org/).
[Minipay](https://www.minipay.io/) looked like it could work (with some banging).
[Ko-fi](https://ko-fi.com/) came closer.

The winner was [BuyMeACoffee](https://www.buymeacoffee.com/).
It has a [comparison to Ko-fi](https://www.buymeacoffee.com/ko-fi-alternative)
that seemed good enough for me (they wouldn't allow it on the internet if it wasn't true, right?).
The most important feature for me: zero cost (since I suspect I'll get zero revenue).

Setting it up was super easy.
Generate the code for a button or widget and add it to your website.
Connect your Stripe and/or PayPal account and you are all set.
When the cash starts rolling in (lol!), BMAC will keep their 5% fee and the rest goes into
your Stripe or PayPal account.

# Next
The way the world (usually) works is you must give value before you get value.
So, this experiment will depend heavily on how much value I give in my upcoming blog posts.
So, that's motiviation for the next step: write some valuable blog posts.

After that, a year or so from now, I’ll write Part 2 of this experiment:
how it worked out and how much I made.
