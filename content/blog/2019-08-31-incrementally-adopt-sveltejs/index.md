+++
draft = true
title = 'How To Incrementally Adopt SvelteJS Into An Existing Website'
tags = ['sveltejs']
+++

It's not obvious from its excellent tutorials, examples, or API doc
but it _is_ possible to incrementally adopt [SvelteJS](https://svelte.dev/)
into an existing web site or app's HTML.  Here's how.
<!--more-->  

If your mind's not boggling yet over [SvelteJS](https://svelte.dev/), go watch
this video, [Rethinking Reactivity](https://www.youtube.com/watch?v=AdNJ3fydeao),
immediately.
Then help save the planet by reducing the carbon footprint of websites
by using SvelteJS instead of those virtual DOM based frameworks
that consume a lot of cpu, memory, and power.
Start adopting SvelteJS today!

## What & Why

## How To

### Needs
First, in HTML5 there is no such thing as a self-closing tag. For `<stock-price/>`,
HTML5 ignores the `/` and everything afterwards becomes child elements of `<stock-price>`.

Simplest use case:
{{< highlight html >}}
<stock-price symbol="GOOG"/></stock-price>
{{< /highlight >}}

{{< highlight html >}}
<div class="stock-price" symbol="GOOG"/>
{{< /highlight >}}

Less simple but very common and desirable use case:
{{< highlight html >}}
<navbar type="left-slider">
  <li><a href="/">Home</a></li>
  <li><a href="/about">Aabout</a></li>
</navbar>
{{< /highlight >}}

{{< highlight html >}}
<div class="bnc-navbar" type="left-slider">
  <li><a href="/">Home</a></li>
  <li><a href="/about">Aabout</a></li>
</div>
{{< /highlight >}}

Complex but it turns out to be NOT all that common.  Or at least can live without it
until website can be fully re-written with SvelteJS.


### Solution

