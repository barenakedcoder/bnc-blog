+++
title = "Hire The Coder"
slug = "hire"
+++

## About

## Projects

The coder is skilled and experienced in many areas (too many to list).
He is currently interested in projects related to the following:

* Consulting to strip, simplify, and re-focus
  requirements, solutions (designs), teams, and projects.
  To be clear, by "strip" I mean removing the unnecessary
  using principles such as
  [YAGNI](https://en.wikipedia.org/wiki/You_aren't_gonna_need_it),
  [Muntzing](https://en.wikipedia.org/wiki/Muntzing),
  [Value Engineering](https://en.wikipedia.org/wiki/Value_engineering),
  [feature de-creeping](https://en.wikipedia.org/wiki/Feature_creep),
  and [KISS](https://en.wikipedia.org/wiki/KISS_principle).
  
* Web development (full-stack )

* Python: Anything in its ecosystem including Data Science and
  Machine Learning.

* Saleforce.com 