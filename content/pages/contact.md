+++
title = "Contact"
slug = "contact"
+++

You can try me at [LinkedIn](https://www.linkedin.com/in/parakin)
or [Twitter](https://twitter.com/BareNakedCoder)
but I'm usually not there.
Your best bet is to complete this form (I'll get an email):

<iframe src="https://docs.google.com/forms/d/e/1FAIpQLSe_VGvHTfU_pBgMfr2K-qZzjf4k2nWVKtn--jor_xpFgWtoIw/viewform?embedded=true" width="600" height="751" frameborder="0" marginheight="0" marginwidth="0"
    style="height:751px;">
    <!-- FIX: added style= so form not short/scrollable -->
 Loading...
</iframe>
