+++
title = "Privacy Policy"
slug = "privacy-policy"
+++

## TLDR;
If you are from a country covered by
[GDPR](https://en.wikipedia.org/wiki/General_Data_Protection_Regulation)
or similar inscrutable laws, please leave this website immediately.

## Policy
(WORK-IN-PROGRESS)

This website does use 3rd party plug-ins:
Google Analytics (for web traffic statistics),
Google Forms (for contacting me),
and Disqus (for comments).

This website is a static website (no run-time, no database).
So, except for the 3rd party plug-ins above, it does not use
cookies or any other means of identifying you.

I really have zero interest in doing anything to you that's annoying (like marketing) or evil (like fraud).
Until I figure out all these damn regulations, I hope these disclaimers are enough to keep the privacy police away.
Please don't sue me.  Pretty please.

--------
For Google Analytics, 
see [how Google uses data when you use our partners' sites or apps](https://www.google.com/policies/privacy/partners/).

<!-- TODO: Copy from https://www.thoughtworks.com/privacy-policy -->
