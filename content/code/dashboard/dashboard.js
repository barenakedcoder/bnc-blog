'use strict';
/*! dashboard.js 2.0 | (c) 2019 - Don Parakin, BareNakedCoder.com | MIT License */

const DASHBOARD_DATA_TAG_ID = 'dashboard-data';
const $G = {
    TITLE: '',
    THIS_FILE: null,
    HEADER_COLOR: '#000',
    HEADER_BACKGROUND_COLOR: '#a6be8e',
    n_persons: 0,
    n_details: 0,
    n_tabsets: 0,
    handlers: null
};

window.addEventListener('DOMContentLoaded', e=>{
    $G.THIS_FILE = window.location.pathname.replace(/^\/+/, '');
    $G.handlers = [Comment, Link, Tabs, Columns, Heading, Break, Person,
        DropDown, Notes, DaysUntil, Progress, Calendar, SetValue];
    let r = getDashboardData();
    if (r.ok) r = parseDashboardData(r.dbd);
    if (r.ok) r = renderHtml(r.rootNode);
    r.html = SetValue.substitute(r.html);
    if (r.ok) r = writeHtml(r.html);
    if (r.ok) r = runJS();
});

function getDashboardData() {
    let dataElement = document.getElementById(DASHBOARD_DATA_TAG_ID);
    if (!dataElement) {
        alert(`<script id="${DASHBOARD_DATA_TAG_ID}">...</script> was not found.`);
        return {ok:false};
    }
    let dbd = new DashboardData(dataElement.innerHTML);
    return {ok:true, dbd:dbd};
}

function parseDashboardData(dbd) {
    let rootNode = new Root();
    let currentNode = rootNode;
    while (dbd.hasLines()) {
        dbd.next();
        let nextNode = false;
        for (let handler of $G.handlers) {
            nextNode = handler.parse(dbd, currentNode);
            if (nextNode) break;
        }
        if (nextNode) {
            currentNode = nextNode;
        } else {
            console.log(`ERROR: Line ${dbd.currentLine}: Invalid directive:\n\t${dbd.line.trim()}`);
        }
    }
    return {ok:true, rootNode:rootNode};
}

function renderHtml(rootNode) {
    document.title = $G.TITLE;
    let css = [PageParts.getCSS()];
    $G.handlers.forEach(hc=>css.push(hc.getCSS()));
    let html = [];
    PageParts.renderHeader(html);
    PageParts.renderEmailListModal(html);
    rootNode.render(html);
    PageParts.renderFooter(html);
    let body_html = `<style>${css.join('')}</style>${html.join('')}`;
    return {ok:true, html:body_html};
}

function writeHtml(html) {
    let body = document.getElementsByTagName('body');
    if (body.length>0) {
        body[0].innerHTML = html;
    }
    return {ok:true};
}

function runJS() {
    $G.handlers.forEach(h=>h.runJS());
    return {ok:true};
}


//======================================================================
class DashboardData {
    constructor(data) {
        this.lines = data.split('\n');
        this.currentLine = -1;
        this.errors = [];
    }
    hasLines() {
        return (this.currentLine + 1) < this.lines.length;
    }
    next() {
        this.currentLine += 1;
        this.line = this.lines[this.currentLine];
        let parts = this.line.split(':');
        this.cmd = parts.shift().trim().toLowerCase();
        parts = parts.join(':').split('-->');
        this.args = parts.shift().trim();
        this.dst = parts.join('-->').trim();
        parts = this.args.split('~');
        this.src = parts.shift().trim();
        this.srcArg = parts.join('~').trim();
    }
}

//======================================================================
class Handler {
    constructor(cmd, parent) {
        this.cmd = cmd || '<root>';
        this.parent = parent || null;
        this.children = [];
    }
    add(childNode) {
        childNode.parent = this;
        this.children.push(childNode);
    }
    addToLastSibling(childNode) {
        let sibling = this.children.slice(-1)[0] || this;
        childNode.parent = sibling;
        sibling.children.push(childNode);
    }
    static parse(dbd, currentNode) {}
    static getCSS() { return ''; }
    render(html) {}
    static runJS() {}
}

class Root extends Handler {
    render(html) {
        this.children.forEach(h=>h.render(html));
    }
}

class Comment extends Handler {
    static parse(dbd, currentNode) {
        if (dbd.line.trim()==='' || dbd.cmd.startsWith('//') || dbd.cmd.startsWith('#')) {
            return currentNode;
        }
        return false;
    }
}

class Tabs extends Handler {
    static parse(dbd, currentNode) {
        if (dbd.cmd==='tabs') {
            let newNode = new Tabs(dbd.cmd, null);
            currentNode.add(newNode);
            return newNode;
        } else if (dbd.cmd==='tabs-end') {
            return currentNode.parent;
        } else if (dbd.cmd==='tab') {
            let newNode = new Tabs(dbd.cmd, dbd.args);
            currentNode.add(newNode);
            return newNode;
        } else if (dbd.cmd==='tab-end') {
            return currentNode.parent;
        }
        return false;
    }
    constructor(cmd, text) {
        super(cmd);
        this.text = text;
    }
    static getCSS() {
        return `
        .db-tabs { margin:1rem 1rem; }
        .db-tabs > .bar { display:flex; flex-flow:row wrap; margin-bottom:1rem; }
        .db-tabs > .bar > div { flex:none; }
        .db-tabs > .bar > div.filler { flex:1; }
        .db-tabs > .bar > div.filler label { background-color:#fff; }
        .db-tabs > .bar input { display:none; }
        .db-tabs label { bad_display:inline-block; padding:.25rem 1rem;
            border:1px solid; text-align:center;
            background-color:#eee; border-color:#fff #fff #222 #fff; }
        .db-tabs input:checked + label { border: 1px solid; border-width: 1px 1px 0 1px;
            background-color:#fff; border-color:#222 #222 #fff #222; }
        .db-tabs label:hover { cursor: pointer; background-color:#fff; }
        .db-tabs .db-tabs-content { display:none; }
        `;
    }
    render(html) {
        if (this.cmd==='tabs') {
            this.render_tabs(html);
        } else if (this.cmd==='tab') {
            this.render_tab(html);
        }
    }
    render_tabs(html) {
        let n_tabset = ++$G.n_tabsets;
        html.push(`<div id="db-tabset-${n_tabset}" class="db-tabs">`);
        //--------- Tab Button Bar
        let tab_content = [];
        html.push('<div class="bar">');
        let n_tab = 1;
        for (const child of this.children.filter(c=>c.cmd==='tab')) {
            let tab_id = `tab-${n_tabset}-${n_tab}`;
            html.push(`
                <div data-tab="${n_tabset}-${n_tab}">
                <input id="${tab_id}" type="radio" name="tabs">
                <label for="${tab_id}" class="db-tabs-label">${child.text}</label>
                </div>
            `);
            tab_content.push(`<div id="${tab_id}-content" class="db-tabs-content">`);
            child.render(tab_content);
            tab_content.push('</div>');
            n_tab += 1;
        }
        //console.log('render_tabs: tab_content=', tab_content);
        html.push('<div class="filler"><label>&nbsp;</label></div>');
        html.push('</div>');  // ends "bar"
        tab_content.forEach(tc=>html.push(tc));
        html.push('</div>');  // ends "db-tabs"
    }
    render_tab(html) {
        this.children.forEach(c=>c.render(html));
    }
    static runJS() {
        for (let i=1; i<=$G.n_tabsets; i++) {
            let e = document.getElementById(`tab-${i}-1`);
            if (e) { e.checked = true; }
            e = document.getElementById(`tab-${i}-1-content`);
            if (e) { e.style.display = 'block'; }
        }
        document.querySelectorAll('.db-tabs label').forEach(label=>{
            label.addEventListener('click', event=>{
                let tab_id = label.getAttribute('for');
                let ids = (tab_id || 'tab-0-0').split('-');
                let tabset_id = `db-tabset-${ids[1]}`;
                let tabset = document.getElementById(tabset_id);
                if (!tabset) {
                    console.log(`ERROR: Tabs: "#${tabset_id}" not found.`);
                    return;
                }
                let contents = tabset.querySelectorAll('.db-tabs-content');
                contents.forEach(c=>c.style.display='none');
                let e = document.getElementById(`${tab_id}-content`);
                if (e) { e.style.display = 'block'; }
            });
        });
    }
}

class Columns extends Handler {
    static parse(dbd, currentNode) {
        if (dbd.cmd==='columns') {
            let newNode = new Columns(dbd.cmd);
            currentNode.add(newNode);
            return newNode;
        } else if (dbd.cmd==='columns-end') {
            return currentNode.parent;
        } else if (dbd.cmd==='column') {
            let newNode = new Columns(dbd.cmd);
            currentNode.add(newNode);
            return newNode;
        } else if (dbd.cmd==='column-end') {
            return currentNode.parent;
        }
        return false;
    }
    static getCSS() {
        return `
        .db-columns { display:flex; flex-flow:row wrap; }
        .db-columns > div { flex:1 0 0; width:1rem; margin-right:1rem; }
        .db-columns > :last-child { margin-right:0; }
        `;
    }
    render(html) {
        if (this.cmd==='columns') {
            this.render_columns(html);
        } else if (this.cmd==='column') {
            this.render_column(html);
        }
    }
    render_columns(html) {
        html.push('<div class="db-columns">');
        this.children.forEach(c=>c.render(html));
        html.push('</div>');
    }
    render_column(html) {
        html.push('<div>');
        this.children.forEach(c=>c.render(html));
        html.push('</div>');
    }
}

class Heading extends Handler {
    static parse(dbd, currentNode) {
        if (['h1','brh1','h2','brh2'].includes(dbd.cmd)) {
            currentNode.children.push(new Heading(dbd.cmd, dbd.args));
            return currentNode;
        }
        return false;
    }
    constructor(cmd, text) {
        super(cmd);
        this.text = text;
    }
    static getCSS() {
        return `
        h1, h2 { font-size:100%; font-weight:bold; margin:0; }
        h1 { border-top:2px solid #444; padding:.25rem .5rem;
            color:${$G.HEADER_COLOR}; background-color:${$G.HEADER_BACKGROUND_COLOR}; }
        h2 { border-top:1px solid #444; padding:.1rem .5rem;
            color:#222; background-color:#eee; }
        `;
    }
    render(html) {
        let tag = this.cmd;
        if ( tag.startsWith('br')) {
            html.push('<br>');
            tag = tag.slice(2);
        }
        html.push(`<${tag}>${this.text || '&nbsp;'}</${tag}>`);
    }
}

class Break extends Handler {
    static parse(dbd, currentNode) {
        if (dbd.cmd==='br') {
            currentNode.children.push(new Break());
            return currentNode;
        }
        return false;
    }
    render(html) {
        html.push('<br>');
    }
}

class Link extends Root {
    static parse(dbd, currentNode) {
        if (dbd.cmd === 'link') {
            let newNode = new Link(dbd.cmd, dbd.src, dbd.dst);
            currentNode.add(newNode);
            return currentNode;
        } else if (dbd.cmd==='sublink') {
            let newNode = new Link(dbd.cmd, dbd.src, dbd.dst);
            currentNode.addToLastSibling(newNode);
            return currentNode;
        }
        return false;
    }
    constructor(cmd, text, url) {
        super(cmd);
        this.text = text;
        this.url = url;
    }
    static getCSS() {
        return `
        a, a:link, a:visited, a:hover, a:active { color:#222; text-decoration:none; }
        .db-link :first-child { padding-right:1rem; }
        .db-link:hover { border:1px solid #888; background-color:#ffffe6;;
            border-radius:4px; -moz-border-radius:4px; cursor:pointer; }
        .db-sublink { padding:0px 4px; border:1px solid #aaa; font-size:85%; border-radius:4px;
            margin:1px 0 1px 4px; background-color:#fff; line-height:1; }
        `;
    }
    render(html) {
        if (this.cmd==='link') {
            this.render_link(html);
        } else if (this.cmd==='sublink') {
            this.render_sublink(html);
        }
    }
    render_link(html) {
        html.push('<div class="db-slot db-link">');
        html.push(`<a href="${this.url}">${this.text}</a>`);
        this.children.forEach(c=>c.render(html));
        //TODO: html += {{notes_html}}
        html.push('</div>');
    }
    render_sublink(html) {
        html.push(` <a class="db-sublink" href="${this.url}">${this.text}</a>`);
    }
}

class Person extends Handler {
    static parse(dbd, currentNode) {
        if (dbd.cmd==='p' || dbd.cmd==='person') {
            $G.n_persons += 1;
            let newNode = new Person('p', dbd.src, dbd.srcArg, dbd.dst);
            currentNode.add(newNode);
            return currentNode;
        }
    }
    constructor(cmd, name, role, email) {
        super(cmd);
        this.name = name;
        this.role = role;
        this.email = email;
    }
    static getCSS() {
        return `
        .db-p-role { font-size:80%; color:#888; }
        `;
    }
    render(html) {
        html.push('<div class="db-slot">');
        html.push('<input class="db-p-cb" type="checkbox"');
        html.push(` data-email="${this.email}"/>${this.name}`);
        if (this.role!=='') {
            html.push(`<span class="db-p-role">, ${this.role}</span>`);
        }
        //TODO: {{notes_html}}
        html.push('</div>');
    }
    static runJS() {
        let btn = document.getElementById('db-p-emails-list');
        if (btn) {
            btn.addEventListener('click', event=>{
                let emails = [];
                document.querySelectorAll('.db-p-cb:checked').forEach(el=>{
                    let em = el.getAttribute('data-email');
                    if (em) emails.push(em);
                });
                if (emails.length===0) emails = ['(none selected)'];
                document.getElementById('db-p-emails-ta').value = emails.join(', ');
                document.getElementById('db-p-emails').style.display = 'block';
            });
        }
        document.getElementById('db-p-emails-ta')
        .addEventListener('focus', event=>{
            event.currentTarget.select();
        });
        document.getElementById('db-p-emails-close')
            .addEventListener('click', event=>{
            document.getElementById('db-p-emails').style.display = 'none';
        });
        document.getElementById('db-p-emails-clear')
            .addEventListener('click', event=>{
            document.getElementById('db-p-emails').style.display = 'none';
            document.querySelectorAll('.db-p-cb:checked').forEach(el=>{
                el.checked = false;
            });
        });
    }
}

class DropDown extends Handler {
    static parse(dbd, currentNode) {
        if (dbd.cmd==='dropdown') {
            let newNode = new DropDown(dbd.cmd, dbd.src);
            currentNode.add(newNode);
            return newNode;
        } else if (dbd.cmd==='dropdown-end') {
            return currentNode.parent;
        }
        return false;
    }
    constructor(cmd, title) {
        super(cmd);
        this.title = title;
    }
    static getCSS() {
        return `
        select.db-dropdown {font-size:100%; xborder:1px solid #bbb; xcolor:#000;
            background-color: #ffffe6; width: 100%; }
        `;
    }
    render(html) {
        html.push(`<div class="db-slot"><select class="db-dropdown">`);
        html.push(`<option value="">${this.title}</option>`);
        this.children.filter(c => c.cmd==='link')
            .forEach(c => html.push(`<option value="${c.url}">${c.text}</option>`));
        html.push('</select></div>')
    }
    static runJS() {
        document.querySelectorAll('.db-dropdown').forEach(s=>{
            s.addEventListener('change', event=>{
                let url = s.options[s.selectedIndex].value;
                if (url !== '') {
                    window.location.href = url;
                }
            });
        });
    }
}

class Notes extends Handler {
    static parse(dbd, currentNode) {
        if (['note','n'].includes(dbd.cmd)) {
            currentNode.add(new Notes(dbd.cmd, dbd.args));
            return currentNode;
        } else if (dbd.cmd==='n') {
            return currentNode;
        }
        return false;
    }
    constructor(cmd, text) {
        super(cmd);
        this.text = text;
    }
    render(html) {
        html.push(`<div class="db-slot">${this.text}</div>`);
    }
}

class DaysUntil extends Handler {
    static parse(dbd, currentNode) {
        if (dbd.cmd==='daysuntil') {
            let newNode = new DaysUntil(dbd.cmd, dbd.src, dbd.dst);
            currentNode.add(newNode);
            return currentNode;
        }
        return false;
    }
    constructor(cmd, date, text) {
        super(cmd);
        this.date = date;
        this.text = text;
    }
    render(html) {
        let text;
        let date = this.date.replace(/\D/g,'');
        if (date.length !== 8) {
            text = this.cmd+': invalid date: '+this.date;
        } else {
            let now = Date.now();
            let then = Date.UTC(date.slice(0,4), date.slice(4,6)-1,
                date.slice(6), 23, 59, 59);
            let diff = Math.floor((then-now)/(1000*60*60*24));
            let DU = `${diff} ${diff===1 ? 'day' : 'days'}`;
            text = this.text.indexOf('{{DU}}') >= 0
                ? this.text.replace('{{DU}}', DU)
                : `${DU} ${this.text}`;
            text = text.replace('{{MD}}', date_fmt(new Date(then),'%b %-d'));
        }
        html.push(`<div class="db-slot">${text}</div>`);
    }
}

class Progress extends Handler {
    static parse(dbd, currentNode) {
        if (dbd.cmd==='progress') {
            let newNode = new Progress(dbd.cmd, dbd.src, dbd.dst);
            currentNode.add(newNode);
            return currentNode;
        }
        return false;
    }
    constructor(cmd, date_start, date_end) {
        super(cmd);
        this.date_start = date_start;
        this.date_end = date_end;
    }
    render(html) {
        let s = date_fmt(this.date_start, '%Y-%m-%dT12:00:00');
        let e = date_fmt(this.date_end, '%Y-%m-%dT12:00:00');
        if (!s) {
            html.push(`<div class="db-slot">progress: invalid date: ${s}</div>`)
        } else if (!e) {
            html.push(`<div class="db-slot">progress: invalid date: ${e}</div>`)
        } else {
            let start = new Date(s);
            let end = new Date(e);
            let diff = end.getTime() - start.getTime();
            let now = new Date();
            let progress = Math.round(100*(now.getTime()-start.getTime())/diff);
            html.push(`<div class="db-slot">Now ${progress}% thru `
                + `${date_fmt(start,'%b %-d')} > ${date_fmt(end,'%b %-d')}</div>`);
        }
    }
}

class Calendar extends Handler {
    static parse(dbd, currentNode) {
        if (dbd.cmd==='calendar') {
            let newNode = new Calendar(dbd.cmd);
            currentNode.add(newNode);
            return newNode;
        } else if (dbd.cmd==='calendar-end') {
            return currentNode.parent;
        } else if (dbd.cmd==='cal') {
            if (!currentNode.hasOwnProperty('entries')) {
                return; 
            }
            let i_date = new Date(date_fmt(dbd.src, '%Y-%m-%d'));
            let repeat = Math.min(30, Number.parseInt(dbd.srcArg) || 1);
            while (repeat) {
                let ymd = date_fmt(i_date);
                if (!currentNode.entries.hasOwnProperty(ymd)) {
                    currentNode.entries[ymd] = [];
                }
                currentNode.entries[ymd].push(dbd.dst);
                currentNode.n_entries += 1;
                if (ymd < currentNode.min) currentNode.min = ymd;
                if (ymd > currentNode.max) currentNode.max = ymd;
                i_date.setDate(1+i_date.getDate());
                --repeat;
            }
            return currentNode;
        }
        return false;
    }
    constructor(cmd) {
        super(cmd);
        this.entries = {};
        this.n_entries = 0;
        this.max = '2000-01-01';
        this.min = '9999-12-31';
    }
    static getCSS() { return `
    .db-calendar { border-bottom:1px solid #ddd; border-right:1px solid #ddd;
        display:flex; flex-flow:row wrap;
    }
    .db-day { border-top:1px solid #ddd; border-left:1px solid #ddd;
        font-size:80%;
        flex:1 1 14%; min-height:4rem; }
    .db-day > div { float:right; padding-right:.5rem; }
    .db-day.we { background-color:#fafafa; }
    .db-day > ul { margin:0 0 0 -20px; list-style: circle outside none; }
    `; }
    render(html) {
        if (this.n_entries === 0) {
            html.push('<p>(empty calendar)</p>');
            return;
        }
        let end_date = new Date(this.max+'T12:00:00');
        end_date.setDate(end_date.getDate()+(6-end_date.getDay()));
        end_date.setHours(23,59,59);
        let end_uts = end_date.getTime();
        let i_date = new Date(this.min+'T12:00:00');
        let n_week = 1;
        i_date.setDate(i_date.getDate()-i_date.getDay());
        html.push('<div class="db-calendar">');
        while (i_date.getTime() < end_uts) {
            let dow = i_date.getDay();
            let dom = i_date.getDate();
            let d_fmt = (dow===0 || dom===1) ? '%b %-d' : '%-d';
            html.push(`<div class="db-day${dow===0||dow===6?' we':''}">`);
            html.push(`<span>${date_fmt(i_date, d_fmt)}</span>`);
            if (dow===6) {
                html.push(` <div>w=${n_week}</div>`);
                n_week += 1;
            }
            let ymd = date_fmt(i_date);
            if (this.entries.hasOwnProperty(ymd)) {
                html.push('<ul>');
                this.entries[ymd].forEach(txt => {
                    html.push(`<li>${txt}</li>`);
                });
                html.push('</ul>');
            }
            html.push('</div>');
            i_date.setDate(i_date.getDate()+1);
        }
        html.push('</div>');
        //console.log('calendar=', this);
    }
}

class SetValue extends Handler {
    static parse(dbd, currentNode) {
        if (dbd.cmd==='set') {
            $G[dbd.src.trim().toUpperCase()] = dbd.dst;
            return currentNode;
        }
        return false;
    }
    static substitute(aString) {
        aString = aString.replace(/{{.+?}}/g, match=>{
            let key = match.slice(2,-2).trim().toUpperCase();
            return $G.hasOwnProperty(key) ? $G[key] : match;
        });
        return aString;
    }
}

class PageParts {
    static getCSS() {
        return `
        html { color:#222; background:#FFF }
        body {font-size:100%; margin:0; font-family:"Open Sans", "Helvetica Neue", Helvetica, Arial, sans-serif; }
        .db-button { background-color:#ffd700; color:#444; border:1px solid #444; cursor:pointer; padding:2px 12px; margin-left:1rem; }
        .db-slot { padding:.25rem; xxxline-height:1;
            border:1px solid #fff; border-top:1px solid #ddd; }

        .db-hdr { display:flex; flex-flow:row nowrap; }
        .db-hdr div { padding:1rem; font-weight:bold; 
            color:${$G.HEADER_COLOR};
            background-color:${$G.HEADER_BACKGROUND_COLOR}; }
        .db-hdr > :nth-child(1) { flex:none; width:8rem; color:#fff; background-color:#666; text-align:center; font-style:italic; }
        .db-hdr > :nth-child(2) { flex:none;}
        .db-hdr > :nth-child(3) { flex:1; text-align:right; font-size:90%; }
        #db-details-switch { display:none; }

        #db-p-emails { display:none; position:absolute; top:125px; left:125px; border:2px solid #666;
            background-color:#ddd; padding:25px;
            box-shadow:12px 12px 18px #aaa; -webkit-box-shadow:12px 12px 18px #aaa; }
        `;
    }
    static renderHeader(html) {
        let head = document.getElementsByTagName('head');
        if ( head && head.length > 0) {
            console.log('adding head.link:');
            let favicon = document.createElement('link');
            favicon.rel='shortcut icon';
            favicon.href = 'https://www.barenakedcoder.com/code/dashboard/dashboard.ico';
            head[0].appendChild(favicon);
        }
        html.push(`
        <div class="db-hdr">
            <div>Dashboard</div>
            <div>{{TITLE}}</div>
            <div>
        `);
        if ($G.n_details > 0) {
            html.push('<span id="db-details-switch" class="db-button">Detail</span>');
        }
        if ($G.n_persons > 0) {
            html.push('<span id="db-p-emails-list" class="db-button">Email List</span>');
        }
        if ($G.THIS_FILE) {
            html.push('<a href="mydocs:open:{{THIS_FILE}}"><span id="db-edit" class="db-button">Edit</span></a>');
        }
        html.push('</div></div>');
    }
    static renderEmailListModal(html) {
        html.push(`
        <div id="db-p-emails">
        List of selected ids for use in email (cut &amp; paste):<br/>
        <textarea id="db-p-emails-ta" rows="12" readonly="readonly" style="width:700px;"></textarea><br/>
        <br/>
        <span id="db-p-emails-close" class="db-button">Close</span>
        <span id="db-p-emails-clear" class="db-button">Close + Clear</span>
        </div>
        `);
    }
    static renderFooter(html) {
        return;
    }
}

function date_fmt(aDate, fmt) {
    const mmm = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
    fmt = fmt || '%Y-%m-%d';
    if (aDate instanceof Date) {
        aDate = aDate.toISOString().slice(0,10);
    }
    let ymd = String(aDate).replace(/\D/g, '');
    if (ymd.length !== 8) {
        return null;
    }
    // Ref: http://strftime.org/
    fmt = fmt.replace('%Y', ymd.slice(0,4));
    fmt = fmt.replace('%m', ymd.slice(4,6));
    fmt = fmt.replace('%d', ymd.slice(6));
    fmt = fmt.replace('%-d', ymd.slice(6).replace(/^0+/, ''));
    fmt = fmt.replace('%b', mmm[ymd.slice(4,6)-1]);
    return fmt;
}
