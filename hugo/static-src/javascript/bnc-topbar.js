/**
 * @file VueJS component for <bnc-topbar> tag
 * Adds the top banner of web pages, including top navigation.
 *  - Based on Bulma navbar menus.
 * @author Don Parakin
 */
import Vue from 'vue';

const vue_tag = 'bnc-topbar';
const vue_props = {};

const vue_template = `   
 <section class="a-topbar">
  <nav class="navbar container" role="navigation" aria-label="main navigation">

   <div class="navbar-brand">
    <a class="navbar-item" href="/">
     <span class="title is-size-3 is-size-4-touch">BareNakedCoder<span class="is-size-5 is-size-6-touch">.com</span></span>
    </a>
    <a role="button" class="navbar-burger burger {is_active}"
        aria-label="menu" aria-expanded="false" href="#toggle"
        @click.prevent="toggleMenu">
     <span aria-hidden="true"></span>
     <span aria-hidden="true"></span>
     <span aria-hidden="true"></span>
    </a>
   </div>

   <div class="navbar-menu pad-touch-only" v-bind:class="{'is-active':isMenuOpen}" id="navbarBasicExample">
    <div class="navbar-end">
     <slot></slot>
    </div>
   </div>
  </nav>
 </section>
`;

const vue_data = function() {
    return {
        isMenuOpen: false,
    };
};

function toggleMenu() {
    this.isMenuOpen = !this.isMenuOpen;
}

/**
 * @function Add this VueJS component to Vue.
 */
export function add_component() {
    Vue.component(vue_tag, {
        //props: vue_props,
        data: vue_data,
        methods: {toggleMenu},
        template: vue_template
    });
}
