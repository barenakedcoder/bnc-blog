/**
 * @file Initialize and start VueJS
 * @author Don Parakin
 */
import Vue from 'vue';
import * as bnc_topbar from './bnc-topbar';
import * as bnc_comments from './bnc-comments';
import * as bnc_money_tip from './bnc-money-tip';

bnc_topbar.add_component();
bnc_comments.add_component();
bnc_money_tip.add_component();

const vueApp = new Vue({
    el: '#vue-app',
    // Hugo-generated HTML has [v ... v] for VueJS (since Hugo itself already uses {{ }} delimters)
    delimiters: ['[v', 'v]']
});
