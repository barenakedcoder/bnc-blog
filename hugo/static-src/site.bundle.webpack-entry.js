/**
 * @file Webpack entry point for the "site" bundle.
 * @author Don Parakin *
 */

import './css/style.scss';
import './javascript/_main';
