
# GAE README

## DEVELOPER SET-UP
- __ Python libraries
  - See ./pylibs/requirements.txt
  - Commands:
    - `cd /path/to/project`
    - `pip install --requirement _app\pylibs\requirements.txt --target vendor`
  - Add to IDE (IntelliJ's) configuration (so it can autosense while coding):
    - File > Project Structure > Project Settings/Libraries
    - Add ./vendor directory under categorie "Classes"
- __ Setup Gulp
  - See ./_dev/gulp/README.md
- __ Setup IDE (IntelliJ) run commands


### Installing / Upgrading the Gcloud SDK

- BUG/FIX: After install or upgrade, get an "ImportError: No module named msvcrt".
  - This is caused because Flask uses Click which detects running on Windows so attempts
    to load a Windows-specific library, "msvcrt", that GAE does not allow.
  - WORKAROUND: Edit <app-root>/appengine_config.py and add code
  - REF:  https://stackoverflow.com/questions/25915164/django-1-7-on-app-engine-importerror-no-module-named-msvcrt

