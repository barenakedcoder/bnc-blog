# bnc-blog

Content and source code (Hugo and GAE) for the barenakedcoder.com web site.

## Overview / Concepts
* Web site has two parts:
  * Static site built by Hugo.
    * See `/content` for the blog content (easy access)
    * See `/hugo` for Hugo setup, config, etc.
    * See `/static-src` for source for CSS, JS (gulp build target to `/hugo/static`)
    * Hugo's build target is `/hugo/public`
  * Dynamic site/APIs on GAE.
    * See `/gae`.
    
## Build
### /assets/js/svelte.bundle.js (& map)
* STEP: "npm run gulp:build" (defined in package.json):
  * CMD: cd gulp && gulp build
    * builds CSS only; no JS.
    * output: '/hugo/static/assets/css/bundle.min.css'  
* STEP: "npm run svelte:build" (defined in package.json):
  * CMD: rollup -c
    * rollup is a dev dependencies in package.json.
    * uses /x-dev/rollup.config.js
    * input: '/hugo/static-src/svelte/_main.js'
    * output: '/hugo/static/assets/js/svelte.bundle.js'